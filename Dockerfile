FROM yarnpkg/node-yarn

COPY package.json yarn.lock ./

RUN yarn install


EXPOSE 8080

